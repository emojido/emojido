# :book: [Emojidō](http://emojido.gitlab.io/)

[![Let's talk on Slack](https://img.shields.io/badge/%F0%9F%92%AC%20chat-Emojid%C5%8D-blue.svg)](https://emojido.slack.com "Let's talk on Slack")

A web application for creating and sharing emoji-made texts. More on [the Facebook page](https://facebook.com/emojido/)

## Licensing

Source code released under the [GNU Affero General Public License v3](LICENSE).