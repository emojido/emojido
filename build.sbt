import WebJs._
import RjsKeys._

name := """Emojido"""

version := "SNAPSHOT"

lazy val root = (project in file(".")).enablePlugins(PlayJava, SbtWeb)

scalaVersion := "2.11.7"

libraryDependencies ++= Seq(
  javaJdbc,
  cache,
  "org.webjars" %% "webjars-play" % "2.5.0-2",
  "uk.co.panaxiom" %% "play-jongo" % "2.0.0-jongo1.3",
  "org.webjars.npm" % "emojione" % "2.2.6",
  "org.webjars" % "bootstrap" % "3.3.6",
  "org.webjars" % "backbonejs" % "1.3.3",
  "org.webjars" % "underscorejs" % "1.8.3",
  "org.webjars" % "momentjs" % "2.18.1",
  "org.webjars" % "requirejs" % "2.3.3",
  "org.webjars" % "clipboard.js" % "1.6.1"

)

Keys.fork in Test := false

TwirlKeys.constructorAnnotations += "@javax.inject.Inject()"

pipelineStages := Seq(rjs, digest)

modules += JS.Object("name" -> "createEmojiText")
modules += JS.Object("name" -> "searchEmojiTexts")
modules += JS.Object("name" -> "showEmojiText")

paths += ("jsroutes" -> ("jsroutes" -> "empty"))
paths += ("templates" -> ("templates" -> "empty"))
paths += ("emojione" -> ("emojione" -> "empty"))
