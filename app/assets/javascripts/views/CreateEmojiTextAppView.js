define(
	[
		'jquery',
		'underscore',
		'backbone',
		'views/GalleryView',
		'views/EditorEmojiView',
		'AppState',
		'jsroutes'
	],
	function (
		$,
		_,
		Backbone,
		GalleryView,
		EditorEmojiView,
		app,
		jsRoutes
	) {
		'use strict';

		return Backbone.View.extend({
		    el: '#app',

			events: {
			    'change .form-rows input': 'setRows',
				'click .form-rows .plus': 'addRow',
				'click .form-rows .minus': 'removeRow',
				'change .form-columns input': 'setColumns',
				'click .form-columns .plus': 'addColumn',
				'click .form-columns .minus': 'removeColumn',
				'click #submit-button': 'submit'
			},

			initialize: function () {
				this.$editorEmojisContainer = $('#editor-emojis-container');
				this.$rows = this.$('.form-rows input');
				this.$columns = this.$('.form-columns input');

				this.listenTo(app.editorEmojis, 'update', this.renderEditorEmojis);
				this.listenTo(app.editorEmojis, 'reset', this.renderEditorEmojis);
				this.listenTo(app.editor, 'change:rows', this.updateEditorRows);
				this.listenTo(app.editor, 'change:columns', this.updateEditorColumns);

				new GalleryView();

				this.renderEditorEmojis();
				this.updateEditorRows();
				this.updateEditorColumns();
			},

			renderEditorEmojis: function () {
				this.$editorEmojisContainer.html('');

				var maxRows = parseInt(app.editor.get('rows'));
				var maxColumns = parseInt(app.editor.get('columns'));

				for (var rowIndex = 0; rowIndex < maxRows; rowIndex++) {
				    this.$editorEmojisContainer.append('<ul class="emoji-row"></ul>');
				    var $rowContainer = this.$editorEmojisContainer.children().last();
				    for (var columnIndex = 0; columnIndex < maxColumns; columnIndex++) {
				        var view = new EditorEmojiView({ model: app.editorEmojis.findWhere({ 'row' : rowIndex, 'column' : columnIndex }) });
				        $rowContainer.append(view.render().el);
				    }
				}
				$("#editor-emojis-container li:first").addClass("emoji-highlighted");
			},

			setRows: function () {
				app.editor.set('rows', parseInt(this.$rows.val()) >= 1 ? parseInt(this.$rows.val()) : 1);
			},

			addRow: function () {
				app.editor.set('rows', parseInt(app.editor.get('rows')) + 1);
			},

			removeRow: function () {
				app.editor.set('rows', parseInt(app.editor.get('rows')) > 1 ? parseInt(app.editor.get('rows')) - 1 : 1);
			},

			setColumns: function () {
				app.editor.set('columns', parseInt(this.$columns.val()) >= 1 ? parseInt(this.$columns.val()) : 1);
			},

			addColumn: function () {
				app.editor.set('columns', parseInt(app.editor.get('columns')) + 1);
			},

			removeColumn: function () {
				app.editor.set('columns', parseInt(app.editor.get('columns')) > 1 ? parseInt(app.editor.get('columns')) - 1 : 1);
			},

			updateEditorRows: function () {
				this.$rows.val(parseInt(app.editor.get('rows')));

				var previousRows = parseInt(app.editor.previous('rows'));
				var rows = parseInt(app.editor.get('rows'));
				var columns = parseInt(app.editor.get('columns'));

				var rowIndex;
				if (rows > previousRows) {
					var newEmojis = [];
					for (rowIndex = previousRows; rowIndex < rows; rowIndex++) {
						for (var columnIndex = 0; columnIndex < columns; columnIndex++) {
							newEmojis.push({
								row: rowIndex,
								column: columnIndex
							});
						}
					}
					app.editorEmojis.add(newEmojis);
				} else {
					for (rowIndex = rows; rowIndex < previousRows; rowIndex++) {
						app.editorEmojis.remove(app.editorEmojis.where({
							row: rowIndex
						}));
					}
				}
			},

			updateEditorColumns: function () {
				this.$columns.val(parseInt(app.editor.get('columns')));

				var previousColumns = parseInt(app.editor.previous('columns'));
				var columns = parseInt(app.editor.get('columns'));
				var rows = parseInt(app.editor.get('rows'));

				var columnIndex;
				if (columns > previousColumns) {
					var newEmojis = [];
					for (columnIndex = previousColumns; columnIndex < columns; columnIndex++) {
						for (var rowIndex = 0; rowIndex < rows; rowIndex++) {
							newEmojis.push({
								row: rowIndex,
								column: columnIndex
							});
						}
					}
					app.editorEmojis.add(newEmojis);
				} else {
					for (columnIndex = columns; columnIndex < previousColumns; columnIndex++) {
						app.editorEmojis.remove(app.editorEmojis.where({
							column: columnIndex
						}));
					}
				}
			},

			submit: function () {
				var maxRows = _.max(app.editorEmojis.pluck('row'));
		        var maxColumns = _.max(app.editorEmojis.pluck('column'));

		        var content = [];

		        for (var rowIndex = 0; rowIndex <= maxRows; rowIndex++) {
					var row = [];
		            for (var columnIndex = 0; columnIndex <= maxColumns; columnIndex++) {
						row.push(app.editorEmojis.findWhere({ 'row' : rowIndex, 'column' : columnIndex }).get('shortname'));
		            }
					content.push(row);
		        }

				$.ajax({
					type: "POST",
					contentType: "application/json",
					dataType: "json",
					url: jsRoutes.api.emojiText.EmojiTextApiController.create().url,
					data: JSON.stringify ({
						title: $('#title').val(),
						content: content
					}),
					processData: false
				}).done(function(data) {
					location.assign(jsRoutes.controllers.EmojiTextController.showEmojiText(data.id).url);
				});
			}
		});
	}
);
