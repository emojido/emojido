define(
	[
		'jquery',
		'underscore',
		'backbone',
		'collections/GalleryEmojis',
		'collections/GalleryCategories',
		'views/GalleryEmojiView',
		'views/GalleryNavView',
		'AppState'
	],
	function ($,
		_,
		Backbone,
		GalleryEmojis,
		GalleryCategories,
		GalleryEmojiView,
		GalleryNavView,
		app
	) {
		'use strict';

		return Backbone.View.extend({
			el: '.select-emoji-modal',

			initialize: function () {
				app.galleryEmojis = new GalleryEmojis();
				app.galleryCategories = new GalleryCategories();
				this.$galleryEmojisContainer = this.$('#gallery-emojis-container');
				this.$galleryNavsContainer = this.$('#gallery-navs-container');

				this.listenTo(app.galleryEmojis, 'reset', this.renderGalleryEmojis);
				this.listenTo(app.galleryCategories, 'update', this.renderGalleryNavs);
				this.listenTo(app.galleryCategories, 'update', this.resetGalleryEmojis);

				this.$el.one('shown.bs.modal', this.loadGalleryCategories);
				this.loadWraps = [];
				this.loadInterval = 0;
			},

			loadGalleryCategories: function () {
				$.getJSON(jsRoutes.controllers.Assets.versioned("data/categorized.json").url)
			    .done(function (data) {
					$('#gallery-loading-alert').fadeOut();
					app.galleryCategories.set(data);
				});
			},

			renderGalleryEmojis: function () {
			    this.$galleryEmojisContainer.html('');

				clearInterval(this.loadInterval);
				this.loadWraps = [];
	            app.galleryEmojis.each(function (emoji, index) {
	                    var view = new GalleryEmojiView({ model: emoji });

	                    this.$galleryEmojisContainer.append(view.renderBlank().el);
						this.loadWraps.push(function () {
							view.render();
						});
					}, this);
				setInterval (function (thisArg) {
					if (thisArg.loadWraps.length > 0) {
						thisArg.loadWraps.shift()();
					} else {
						clearInterval (thisArg.loadInterval);
					}
				}, 50, this);

	            $("#gallery-emojis-container li:first").addClass("emoji-highlighted");
	        },

			renderGalleryNavs: function () {
	            this.$galleryNavsContainer.html('');
	            app.galleryCategories.each(function (category) {
	                    var view = new GalleryNavView({ model: category });
	                    this.$galleryNavsContainer.append(view.render().el);
	                }, this);
	        },

	        resetGalleryEmojis: function () {
	            var shortnames = _(app.galleryCategories.first().get('shortnames', [])).map(function(shortname) {
	                    return {"shortname": shortname};
	                });
	            app.galleryEmojis.reset(shortnames);
	            $('#gallery-category-title').html(app.galleryCategories.first().get('label', ''));
	        }
		});
	}
);
