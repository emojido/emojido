define(
	[
		'jquery',
		'underscore',
		'backbone',
		'emojione',
		'AppState',
		'text!templates/editableEmoji.tpl',
		'text!templates/editableBlankEmoji.tpl',
		'bootstrap'
	],
	function (
		$,
		_,
		Backbone,
		emojione,
		app,
		editableEmojiTemplate,
		editableBlankEmojiTemplate
	) {
		'use strict';

		return Backbone.View.extend({
			tagName:  'li',

			template: _.template(editableEmojiTemplate),

			blankTemplate: _.template(editableBlankEmojiTemplate),

			attributes: {
			    "class" : "emoji-cell emoji-interactive"
			},

			events: {
			    'click .delete-btn': 'clear',
				'click': 'selectEmoji'
			},

			initialize: function () {
	            this.listenTo(this.model, 'change:shortname', this.render);
	        },

			render: function () {
			    if (this.model.get('shortname') !== '') {
			        this.$el.html(emojione.shortnameToImage(this.template(this.model.toJSON())));
			    } else {
			        this.$el.html(this.blankTemplate());
			    }

				return this;
			},

			selectEmoji: function () {
			    $("#editor-emojis-container li:first").removeClass("emoji-highlighted");
			    app.activeEditorEmojiModel = this.model;
			    $('.select-emoji-modal').modal('show');
			},

			clear: function (e) {
			    e.stopPropagation();
	            this.model.set('shortname', '');
	        }
		});
	}
);
