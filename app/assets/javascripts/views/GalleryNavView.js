define(
	[
		'jquery',
		'underscore',
		'backbone',
		'AppState',
		'text!templates/galleryNav.tpl'
	],
	function (
		$,
		_,
		Backbone,
		app,
		galleryNavTemplate
	) {
		'use strict';

		return Backbone.View.extend({
			tagName:  'li',

			template: _.template(galleryNavTemplate),

			attributes: {
			    "role" : "presentation"
			},

			events: {
				'click': 'changeGalleryCategory'
			},

			render: function () {
			    this.$el.html(emojione.shortnameToImage(this.template(this.model.toJSON())));
				return this;
			},

			changeGalleryCategory: function () {
			    var shortnames = _(this.model.get('shortnames', []).map(function(shortname) {
	                    return {"shortname": shortname};
	                })).value();
			    app.galleryEmojis.reset(shortnames);
			    $('#gallery-category-title').html(this.model.get('label', ''));
			}
		});
	}
);
