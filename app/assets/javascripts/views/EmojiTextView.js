define(
	[
		'jquery',
		'underscore',
		'backbone',
		'moment',
		'views/EmojiTextEmojiView',
		'jsroutes',
		'text!templates/emojiText.tpl',
	],
	function (
		$,
		_,
		Backbone,
		moment,
		EmojiTextEmojiView,
		jsRoutes,
		emojiTextTemplate
	) {
		'use strict';

		return Backbone.View.extend({
			tagName:  'li',

			template: _.template(emojiTextTemplate),

			attributes: {
			    "class" : "emoji-text"
			},

			render: function () {
			    this.$el.html(this.template(_.defaults(
					{
						creationDate: moment(this.model.get('creationDate')).fromNow(),
						url: jsRoutes.controllers.EmojiTextController.showEmojiText(this.model.get('id')).url,
						useAsTemplateUrl: jsRoutes.controllers.EmojiTextController.createEmojiText().url + "#template/" + this.model.get('id')
					},
					this.model.toJSON())));
				var $emojisContainer = this.$el.find('.emoji-grid');
				_.each(this.model.get('content'), function (row) {
					$emojisContainer.append('<ul class="emoji-row"></ul>');
					var $rowContainer = $emojisContainer.children().last();
					_.each(row, function (shortname) {
						var view = new EmojiTextEmojiView({ model: {'shortname': shortname} });
						$rowContainer.append(view.render().el);
					});
				});
				return this;
			}
		});
	}
);
