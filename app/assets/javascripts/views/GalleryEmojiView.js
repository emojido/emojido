define(
	[
		'jquery',
		'underscore',
		'backbone',
		'AppState',
		'text!templates/emoji.tpl',
		'text!templates/blankEmoji.tpl'
	],
	function (
		$,
		_,
		Backbone,
		app,
		emojiTemplate,
		blankEmojiTemplate
	) {
		'use strict';

		return Backbone.View.extend({
			tagName:  'li',

			template: _.template(emojiTemplate),

			blankTemplate: _.template(blankEmojiTemplate),

			attributes: {
			    "class" : "emoji-cell emoji-interactive "
			},

			events: {
				'click': 'assignEmoji'
			},

			renderBlank: function () {
				this.$el.html(this.blankTemplate());
				return this;
			},

			render: function () {
				this.$el.html(emojione.shortnameToImage(this.template(this.model.toJSON())));
				return this;
			},

			assignEmoji: function () {
			    $("#gallery-emojis-container li:first").removeClass("emoji-highlighted");
			    app.activeEditorEmojiModel.set('shortname', this.model.get('shortname'));
			    $('.select-emoji-modal').modal('hide');
			}
		});
	}
);
