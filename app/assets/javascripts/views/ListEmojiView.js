define(
	[
		'jquery',
		'emojione',
		'underscore',
		'backbone',
		'text!templates/emoji.tpl',
		'text!templates/blankEmoji.tpl'
	],
	function (
		$,
		emojione,
		_,
		Backbone,
		emojiTemplate,
		blankEmojiTemplate
	) {
		'use strict';

		return Backbone.View.extend({
			tagName:  'li',

			template: _.template(emojiTemplate),

			blankTemplate: _.template(blankEmojiTemplate),

			attributes: {
			    "class" : "emoji-cell"
			},

			render: function () {
			    if (this.model.shortname !== '') {
			        this.$el.html(emojione.shortnameToImage(this.template(this.model)));
			    } else {
			        this.$el.html(this.blankTemplate());
			    }

				return this;
			}
		});
	}
);
