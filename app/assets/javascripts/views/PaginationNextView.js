define(
	[
		'views/PaginationView',
		'AppState'
	],
	function (
		PaginationView,
		app
	) {
		'use strict';

		return PaginationView.extend({
			paginate: function () {
				if (app.pagination.get('size') == app.emojiTexts.length) {
					app.pagination.set('page', app.pagination.get('page') + 1);
				}
			},

			checkIfLimit: function () {
				return app.pagination.get('size') > app.emojiTexts.length;
			}
		});
	}
);
