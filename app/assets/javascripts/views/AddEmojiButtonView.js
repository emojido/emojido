define(
	[
		'jquery',
		'underscore',
		'backbone',
		'AppState',
		'bootstrap'
	],
	function (
		$,
		_,
		Backbone,
		app
	) {
		'use strict';

		return Backbone.View.extend({
			tagName:  'li',

			template: _.template('<span class="glyphicon glyphicon-search emojione" aria-hidden="true"></span>'),

			attributes: {
			    "class" : "emoji-cell emoji-interactive text-center"
			},

			events: {
				'click': 'selectEmoji'
			},

			render: function () {
			    this.$el.html(this.template());
			    if (app.searchEmojis.isEmpty()) {
			        this.$el.addClass("emoji-highlighted");
			    }
			    return this;
			},

			selectEmoji: function () {
			    this.$el.removeClass("emoji-highlighted");
			    $('.select-emoji-modal').modal('show');
			}
		});
	}
);
