define(
	[
		'jquery',
		'backbone',
		'models/Emoji',
		'models/Pagination',
		'views/PaginationPreviousView',
		'views/PaginationNextView',
		'views/SearchEmojiView',
		'views/AddEmojiButtonView',
		'views/EmojiTextView',
		'views/GalleryView',
		'AppState'
	],
	function (
		$,
		Backbone,
		Emoji,
		Pagination,
		PaginationPreviousView,
		PaginationNextView,
		SearchEmojiView,
		AddEmojiButtonView,
		EmojiTextView,
		GalleryView,
		app
	) {
		'use strict';

		return Backbone.View.extend({
		    el: '#app',

			initialize: function () {
				app.activeEditorEmojiModel = new Emoji();
				app.pagination = new Pagination({
					size: app.pageSize
				});

				this.$searchEmojisContainer = $('#search-emojis-container');
	            this.$emojiTextsContainer = $('#emoji-texts-container');

				this.listenTo(app.searchEmojis, 'update', this.renderSearchEmojis);
				this.listenTo(app.searchEmojis, 'update', this.applySearchFilter);
				this.listenTo(app.searchEmojis, 'update', this.resetPagination);
				this.listenTo(app.pagination, 'change', this.applySearchFilter);
	            this.listenTo(app.activeEditorEmojiModel, 'change:shortname', this.addSearchEmoji);
	            this.listenTo(app.emojiTexts, 'update', this.renderEmojiTexts);

				new PaginationPreviousView({ el: $('#pager-top .previous') });
				new PaginationNextView({ el: $('#pager-top .next') });
				new PaginationPreviousView({ el: $('#pager-bottom .previous') });
				new PaginationNextView({ el: $('#pager-bottom .next') });
				new GalleryView();

				this.renderSearchEmojis();
			},

			renderSearchEmojis: function () {
	            this.$searchEmojisContainer.html('');
	            app.searchEmojis.each(function (emoji) {
	                    var view = new SearchEmojiView({ model: emoji });
	                    this.$searchEmojisContainer.append(view.render().el);
	                }, this);
	            var addEmojiButtonView = new AddEmojiButtonView();
	            this.$searchEmojisContainer.append(addEmojiButtonView.render().el);
	        },

			applySearchFilter: function () {
				app.router.navigate("q/" + app.searchEmojis.pluck("shortname").join(",") + "/" + app.pagination.get('page'), {trigger: true});
	        },

			resetPagination: function () {
				app.pagination.set('page', 0, {silent: true});
			},

	        addSearchEmoji: function () {
				if (_.isUndefined(app.searchEmojis.findWhere({shortname: app.activeEditorEmojiModel.get('shortname')}))) {
					app.searchEmojis.add(app.activeEditorEmojiModel.clone());
					app.activeEditorEmojiModel.clear({silent: true});
				}
	        },

	        renderEmojiTexts: function () {
	            this.$emojiTextsContainer.html('');
	            app.emojiTexts.each(function (emojiText) {
	                    var view = new EmojiTextView({ model: emojiText });
	                    this.$emojiTextsContainer.append(view.render().el);
	                }, this);
	        }
		});
	}
);
