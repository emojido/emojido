define(
	[
		'backbone',
		'AppState'
	],
	function (
		Backbone,
		app
	) {
		'use strict';

		return Backbone.View.extend({
			events: {
				'click': 'paginate'
			},

			initialize: function () {
				this.listenTo(app.emojiTexts, 'update', this.updateView);
				this.updateView();
			},

			paginate: function () {},

			updateView: function () {
				if (this.checkIfLimit()) {
					this.$el.addClass('disabled').addClass('hidden');
				} else {
					this.$el.removeClass('disabled').removeClass('hidden');
				}
			},

			checkIfLimit: function () {}
		});
	}
);
