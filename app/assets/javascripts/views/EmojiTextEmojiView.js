define(
	[
		'jquery',
		'underscore',
		'backbone',
		'AppState',
		'text!templates/emoji.tpl',
		'text!templates/blankEmoji.tpl'
	],
	function (
		$,
		_,
		Backbone,
		app,
		emojiTemplate,
		blankEmojiTemplate
	) {
		'use strict';

		return Backbone.View.extend({
			tagName:  'li',

			template: _.template(emojiTemplate),

			blankTemplate: _.template(blankEmojiTemplate),

			attributes: {
			    "class" : "emoji-cell"
			},

			events: {
				'click': 'addToSearchEmojis'
			},

			render: function () {
			    if (this.model.shortname !== '') {
			        this.$el.html(emojione.shortnameToImage(this.template(this.model)));
					this.$el.addClass('emoji-interactive');
			    } else {
			        this.$el.html(this.blankTemplate());
			    }

				return this;
			},

			addToSearchEmojis: function () {
				if (this.model.shortname !== '' && _.isUndefined(app.searchEmojis.findWhere({shortname: this.model.shortname}))) {
					app.searchEmojis.add({
						'shortname': this.model.shortname
					});
				}
			}
		});
	}
);
