define(
	[
		'jquery',
		'underscore',
		'backbone',
		'emojione',
		'text!templates/editableEmoji.tpl',
	],
	function (
		$,
		_,
		Backbone,
		emojione,
		editableEmojiTemplate
	) {
		'use strict';

		return Backbone.View.extend({
			tagName:  'li',

			template: _.template(editableEmojiTemplate),

			events: {
	            'click .delete-btn': 'clear'
	        },

			attributes: {
			    "class" : "emoji-cell emoji-interactive"
			},

			render: function () {
			    this.$el.html(emojione.shortnameToImage(this.template(this.model.toJSON())));
				return this;
			},

			clear: function () {
	            this.model.trigger('destroy', this.model);
	        }
		});
	}
);
