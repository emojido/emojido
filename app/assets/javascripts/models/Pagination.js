define(
	[
		'backbone'
	],
	function (
		Backbone
	) {
		'use strict';

		return Backbone.Model.extend({
			defaults: {
			    page: 0,
				size: 0
			}
		});
	}
);
