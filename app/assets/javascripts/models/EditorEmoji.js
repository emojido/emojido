define(
	[
		'backbone'
	],
	function (
		Backbone
	) {
		'use strict';

		return Backbone.Model.extend({
			defaults: {
			    shortname: '',
				row: 0,
				column: 0
			}
		});
	}
);
