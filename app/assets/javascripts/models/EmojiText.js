define(
	[
		'backbone'
	],
	function (
		Backbone
	) {
		'use strict';

		return Backbone.Model.extend({
			defaults: {
			    id: '',
			    title: '',
			    creationDate: 0,
				content: []
			}
		});
	}
);
