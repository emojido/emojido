define(
	[
		'backbone'
	],
	function (
		Backbone
	) {
		'use strict';

		return Backbone.Model.extend({
			defaults: {
				rows: 0,
				columns: 0
			}
		});
	}
);
