requirejs(
    [
        'emojione',
        'views/CreateEmojiTextAppView',
        'models/Editor',
        'collections/EditorEmojis',
        'routers/CreateEmojiTextRouter',
        'AppState',
        'jsroutes',
    ],
    function (
        emojione,
        AppView,
        Editor,
        EditorEmojis,
        Router,
        app,
        jsRoutes
    ) {
        'use strict';

        emojione.imageType = 'png';
        emojione.imagePathPNG = jsRoutes.controllers.Assets.versioned("lib/emojione/assets/png/").url;

        app.editor = new Editor({
            rows: 3,
            columns: 3
        });
        app.editorEmojis = new EditorEmojis(_.flatten(_.map(
            [
                ['','',''],
                ['','',''],
                ['','','']
            ], function (row, rowIndex) {
                return _.map(row, function (item, columnIndex) {
                    return {"shortname" : item, "row" : rowIndex, "column" : columnIndex};
                });
            })));

        new AppView();

        new Router();
        Backbone.history.start();
    }
);
