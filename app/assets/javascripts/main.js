requirejs.config({
    paths: {
        jquery: "../lib/jquery/jquery",
        underscore: "../lib/underscorejs/underscore",
        emojione: "../lib/emojione/lib/js/emojione",
        backbone: "../lib/backbonejs/backbone",
        bootstrap: "../lib/bootstrap/js/bootstrap",
        moment: "../lib/momentjs/moment",
        clipboard: "../lib/clipboard.js/clipboard",
        text: "../vendor/text/2.0.15/text"
    },
    shim: {
        jquery: {
            exports: "$"
        },
        underscore: {
            exports: "_"
        },
        emojione: {
            exports: "emojione"
        },
        backbone: {
            deps: ["underscore"],
            exports: "Backbone"
        },
        bootstrap: {
            deps: ["jquery"]
        },
        moment: {
            exports: "moment"
        },
        clipboard: {
            exports: "Clipboard"
        }
    }
});
