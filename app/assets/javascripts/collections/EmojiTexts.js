define(
	[
		'backbone',
		'models/EmojiText'
	],
	function (
		Backbone,
		EmojiText
	) {
		'use strict';

		return Backbone.Collection.extend({
			model: EmojiText
		});
	}
);
