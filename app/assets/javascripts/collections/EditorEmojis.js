define(
	[
		'backbone',
		'models/EditorEmoji'
	],
	function (
		Backbone,
		EditorEmoji
	) {
		'use strict';

		return Backbone.Collection.extend({
			model: EditorEmoji
		});
	}
);
