define(
	[
		'backbone',
		'models/Emoji'
	],
	function (
		Backbone,
		Emoji
	) {
		'use strict';

		return Backbone.Collection.extend({
			model: Emoji
		});
	}
);
