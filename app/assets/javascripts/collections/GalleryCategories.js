define(
	[
		'backbone',
		'models/GalleryCategory'
	],
	function (
		Backbone,
		GalleryCategory
	) {
		'use strict';

		return Backbone.Collection.extend({
			model: GalleryCategory
		});
	}
);
