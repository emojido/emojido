define(
	[
		'jquery',
		'underscore',
		'backbone',
		'jsroutes',
        'AppState'
	],
	function (
		$,
		_,
		Backbone,
		jsRoutes,
        app
	) {
		'use strict';

		return Backbone.Router.extend({
			routes:  {
                "q/(:codes)/(:page)": "filter"
            },

			filter: function (codes, page) {
				codes = codes !== null ? codes.split(',') : [];
				page = page !== null ? page : 0;

                $.getJSON (jsRoutes.api.emojiText.EmojiTextApiController.list().url, {
					"codes": codes,
					"page": page
				})
                .done(function(data) {
					app.emojiTexts.set(data);
					app.searchEmojis.set(_.map(codes, function (shortname) {
						return {
							"shortname": shortname
						};
					}));
                });
            }
		});
	}
);
