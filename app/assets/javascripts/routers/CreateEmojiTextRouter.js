define(
	[
		'jquery',
		'underscore',
		'backbone',
		'jsroutes',
        'AppState'
	],
	function (
		$,
		_,
		Backbone,
		jsRoutes,
        app
	) {
		'use strict';

		return Backbone.Router.extend({
			routes:  {
                "template/:id": "template"
            },

			template: function (id) {
                $.getJSON (jsRoutes.api.emojiText.EmojiTextApiController.read(id).url)
                .done(function(data) {
					$('#title').val(data.title);
					app.editor.set('rows', data.content.length);
					app.editor.set('columns', data.content[0].length);
                    app.editorEmojis.reset(_.flatten(_.map(data.content, function (row, rowIndex) {
    					return _.map(row, function (item, columnIndex) {
    						return {"shortname" : item, "row" : rowIndex, "column" : columnIndex};
    					});
    				})));
                });
            }
		});
	}
);
