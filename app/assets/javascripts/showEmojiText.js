/* globals serialized */

requirejs(
    [
        'jquery',
        'emojione',
        'underscore',
        'moment',
        'clipboard',
        'views/ListEmojiView',
        'jsroutes',
        'bootstrap'
    ],
    function (
        $,
        emojione,
        _,
        moment,
        Clipboard,
        ListEmojiView,
        jsRoutes
    ) {
        'use strict';

        emojione.imageType = 'png';
        emojione.imagePathPNG = jsRoutes.controllers.Assets.versioned("lib/emojione/assets/png/").url;

        var $container = $('.emoji-grid');

        _.each(serialized.content, function (row) {
    		$container.append('<ul class="emoji-row"></ul>');
    		var $rowContainer = $container.children().last();
    		_.each(row, function (shortname) {
    			var view = new ListEmojiView({ model: {'shortname': shortname} });
    			$rowContainer.append(view.render().el);
    		});
    	});

        $('#title').text(serialized.title !== "" ? serialized.title : "Untitled");
        $('.creationDate').text(moment(serialized.creationDate).fromNow());

        new Clipboard('.btn-copy-shortcodes', {
            text: function (triger) {
                return _.reduce(serialized.content, function (memo, row) {
                    return memo + _.reduce(row, function (memo, shortcode) {
                        if (shortcode !== "") {
                            return memo + ":" + shortcode + ":";
                        } else {
                            return memo + " ";
                        }
                    }, "") + "\n";
                }, "");
            }
        });

        new Clipboard('.btn-copy-unicode', {
            text: function (triger) {
                return _.reduce(serialized.content, function (memo, row) {
                    return memo + _.reduce(row, function (memo, shortcode) {
                        if (shortcode !== "") {
                            return memo + emojione.shortnameToUnicode(":" + shortcode + ":");
                        } else {
                            return memo + " ";
                        }
                    }, "") + "\n";
                }, "");
            }
        });
    }
);
