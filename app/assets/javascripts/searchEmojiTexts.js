/* globals pageSize */

requirejs(
    [
        'emojione',
        'clipboard',
        'views/SearchEmojiTextsAppView',
        'collections/EmojiTexts',
        'collections/SearchEmojis',
        'routers/SearchEmojiTextsRouter',
        'jsroutes',
        'AppState'
    ],
    function (
        emojione,
        Clipboard,
        AppView,
        EmojiTexts,
        SearchEmojis,
        Router,
        jsRoutes,
        app
    ) {
        'use strict';

        emojione.imageType = 'png';
        emojione.imagePathPNG = jsRoutes.controllers.Assets.versioned("lib/emojione/assets/png/").url;

        app.pageSize = pageSize;
        app.emojiTexts = new EmojiTexts ();
        app.searchEmojis = new SearchEmojis ();

    	new AppView();

        app.router = new Router();
        Backbone.history.start();
        app.router.navigate("q//", {trigger: true, replace: true});

        new Clipboard('.btn-copy-shortcodes', {
            text: function (triger) {
                return _.reduce(app.emojiTexts.findWhere({
                    "id": $(triger).data("id")
                }).get('content'), function (memo, row) {
                    return memo + _.reduce(row, function (memo, shortcode) {
                        if (shortcode !== "") {
                            return memo + ":" + shortcode + ":";
                        } else {
                            return memo + " ";
                        }
                    }, "") + "\n";
                }, "");
            }
        });

        new Clipboard('.btn-copy-unicode', {
            text: function (triger) {
                return _.reduce(app.emojiTexts.findWhere({
                    "id": $(triger).data("id")
                }).get('content'), function (memo, row) {
                    return memo + _.reduce(row, function (memo, shortcode) {
                        if (shortcode !== "") {
                            return memo + emojione.shortnameToUnicode(":" + shortcode + ":");
                        } else {
                            return memo + " ";
                        }
                    }, "") + "\n";
                }, "");
            }
        });
    }
);
