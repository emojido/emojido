<div class="page-header">
    <h2><a href="<%= url %>"><% if (title != '') {print(title)} else {print('Untitled')}; %></a></h2>
    <div class="subtitle-area">
        <span class="glyphicon glyphicon-time" aria-hidden="true"></span> <%= creationDate %>
        <div class="btn-group btn-group-sm" aria-label="Copy">
            <button type="button" class="btn btn-success dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><span class="glyphicon glyphicon-copy" aria-hidden="true"></span> Copy <span class="caret"></span></button>
            <ul class="dropdown-menu">
                <li><a href="<%= useAsTemplateUrl %>">Use as template</a></li>
                <li role="separator" class="divider"></li>
                <li><a href="#" class="btn-copy-shortcodes" role="button" data-id="<%= id %>">Copy as shortcodes</a></li>
                <li><a href="#" class="btn-copy-unicode" role="button" data-id="<%= id %>">Copy as unicode</a></li>
            </ul>
        </div>
    </div>
</div>
<div class="emoji-grid"></div>
