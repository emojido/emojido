/*
 * Emojidō is a web application for creating and sharing emoji-made texts
 * Copyright (C) 2016 Juan Carlos Mejías Rodríguez
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package resources;

import models.EmojiText;

import java.util.List;

public class EmojiTextResource {
    private String id;
    private String title;
    private Long creationDate;
    private List<List<String>> content;

    public EmojiTextResource() {
    }

    public EmojiTextResource(String id, String title, Long creationDate, List<List<String>> content) {
        this.id = id;
        this.title = title;
        this.creationDate = creationDate;
        this.content = content;
    }

    public EmojiTextResource(EmojiText emojiText) {
        this.id = emojiText.id.toHexString();
        this.title = emojiText.title;
        this.creationDate = emojiText.creationDate.getTime();
        this.content = emojiText.content;
    }

    public String getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public Long getCreationDate() {
        return creationDate;
    }

    public List<List<String>> getContent() {
        return content;
    }
}
