/*
 * Emojidō is a web application for creating and sharing emoji-made texts
 * Copyright (C) 2016 Juan Carlos Mejías Rodríguez
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package api.emojiText;

import com.fasterxml.jackson.databind.JsonNode;
import controllers.forms.SearchForm;
import handlers.EmojiTextResourceHandler;
import play.Configuration;
import play.data.Form;
import play.data.FormFactory;
import play.libs.Json;
import play.mvc.Controller;
import play.mvc.Result;
import play.mvc.Results;
import resources.EmojiTextResource;

import javax.inject.Inject;
import java.util.List;
import java.util.concurrent.CompletionStage;
import java.util.stream.Collectors;

public class EmojiTextApiController extends Controller {
    private Configuration configuration;
    private FormFactory formFactory;
    private EmojiTextResourceHandler handler;

    @Inject
    public EmojiTextApiController(
            Configuration configuration,
            FormFactory formFactory,
            EmojiTextResourceHandler handler
    ) {
        this.configuration = configuration;
        this.formFactory = formFactory;
        this.handler = handler;
    }

    public CompletionStage<Result> list() {
        Form<SearchForm> searchForm = formFactory.form(SearchForm.class);
        SearchForm bindedSearchForm = searchForm.bindFromRequest().get();

        int page = bindedSearchForm.getPage();
        int pageSize = configuration.getInt("pageSize", 10);

        if (bindedSearchForm.getCodes().isEmpty()) {
            return handler
                    .list(page, pageSize)
                    .thenApplyAsync(emojiTexts -> {
                        final List<EmojiTextResource> emojiTextsList = emojiTexts.collect(Collectors.toList());
                        return ok(Json.toJson(emojiTextsList));
                    });
        } else {
            return handler
                    .listByCodes(bindedSearchForm
                            .getCodes(), page, pageSize)
                    .thenApplyAsync(emojiTexts -> {
                        final List<EmojiTextResource> emojiTextsList = emojiTexts.collect(Collectors.toList());
                        return ok(Json.toJson(emojiTextsList));
                    });
        }
    }

    public CompletionStage<Result> create() {
        JsonNode json = request().body().asJson();
        final EmojiTextResource emojiTextResource = Json.fromJson(json, EmojiTextResource.class);
        return handler
                .create(emojiTextResource)
                .thenApplyAsync(savedEmojiTextResource -> created(Json.toJson(savedEmojiTextResource)));
    }

    public CompletionStage<Result> read(String id) {
        return handler
                .get(id)
                .thenApplyAsync(optionalEmojiTextResource -> {
                    return optionalEmojiTextResource.map(emojiTextResource ->
                            ok(Json.toJson(emojiTextResource))
                    ).orElseGet(Results::notFound);
                });
    }

    public Result update(String id) {
        return status(NOT_IMPLEMENTED);
    }

    public Result delete(String id) {
        return status(NOT_IMPLEMENTED);
    }
}
