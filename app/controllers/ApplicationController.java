/*
 * Emojidō is a web application for creating and sharing emoji-made texts
 * Copyright (C) 2016 Juan Carlos Mejías Rodríguez
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package controllers;

import play.mvc.Controller;
import play.mvc.Result;
import play.routing.JavaScriptReverseRouter;
import views.html.ApplicationController.about;

public class ApplicationController extends Controller {

    public Result javascriptRoutes() {
        return ok(
                JavaScriptReverseRouter.create("jsRoutes",
                        routes.javascript.Assets.versioned(),
                        routes.javascript.EmojiTextController.searchEmojiTexts(),
                        routes.javascript.EmojiTextController.showEmojiText(),
                        routes.javascript.EmojiTextController.createEmojiText(),
                        api.emojiText.routes.javascript.EmojiTextApiController.list(),
                        api.emojiText.routes.javascript.EmojiTextApiController.create(),
                        api.emojiText.routes.javascript.EmojiTextApiController.read()
                )
        ).as("text/javascript");
    }

    public Result about() {
        return ok(about.render());
    }

}
