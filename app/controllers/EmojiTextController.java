/*
 * Emojidō is a web application for creating and sharing emoji-made texts
 * Copyright (C) 2016 Juan Carlos Mejías Rodríguez
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package controllers;

import resources.EmojiTextResource;
import handlers.EmojiTextResourceHandler;
import play.Configuration;
import play.mvc.Controller;
import play.mvc.Result;
import play.mvc.Results;
import utils.SpriteGenerator;
import views.html.EmojiTextController.createEmojiText;
import views.html.EmojiTextController.searchEmojiTexts;
import views.html.EmojiTextController.showEmojiText;

import javax.inject.Inject;
import java.util.Optional;
import java.util.concurrent.ExecutionException;

public class EmojiTextController extends Controller {
    private EmojiTextResourceHandler handler;
    private SpriteGenerator spriteGenerator;
    private Configuration configuration;
    private WebJarAssets webJarAsset;
    private showEmojiText showEmojiTextView;
    private searchEmojiTexts searchEmojiTextsView;
    private createEmojiText createEmojiTextView;

    @Inject
    public EmojiTextController(
            EmojiTextResourceHandler handler,
            SpriteGenerator spriteGenerator,
            Configuration configuration,
            WebJarAssets webJarAsset,
            showEmojiText showEmojiTextView,
            searchEmojiTexts searchEmojiTextsView,
            createEmojiText createEmojiTextView
    ) {
        this.handler = handler;
        this.spriteGenerator = spriteGenerator;
        this.configuration = configuration;
        this.webJarAsset = webJarAsset;
        this.showEmojiTextView = showEmojiTextView;
        this.searchEmojiTextsView = searchEmojiTextsView;
        this.createEmojiTextView = createEmojiTextView;
    }

    public Result createEmojiText() {
        return ok(createEmojiTextView.render());
    }

    public Result showEmojiText(String id) {
        try {
            Optional<EmojiTextResource> optionalEmojiTextResource = handler
                    .get(id)
                    .toCompletableFuture().get();
            return optionalEmojiTextResource.map(emojiTextResource ->
                    ok(showEmojiTextView.render(emojiTextResource, spriteGenerator.calculateSpriteWidth(emojiTextResource.getContent()), spriteGenerator.calculateSpriteHeight(emojiTextResource.getContent())))
            ).orElseGet(Results::notFound);
        } catch (InterruptedException | ExecutionException e) {
            return internalServerError();
        }
    }

    public Result searchEmojiTexts() {
        return ok(searchEmojiTextsView.render());
    }

    public Result showEmojiTextAsSprite(String id) {
        try {
            Optional<EmojiTextResource> optionalEmojiTextResource = handler
                    .get(id)
                    .toCompletableFuture().get();
            return optionalEmojiTextResource.map(emojiTextResource ->
                    ok(spriteGenerator.generate(emojiTextResource.getContent()))
            ).orElseGet(Results::notFound);
        } catch (InterruptedException | ExecutionException e) {
            return internalServerError();
        }
    }
}
