package controllers.forms;

import java.util.ArrayList;
import java.util.List;

public class SearchForm {
    private List<String> codes = new ArrayList();
    private int page = 0;

    public List<String> getCodes() {
        return codes;
    }

    public void setCodes(List<String> codes) {
        this.codes = codes;
    }

    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }
}
