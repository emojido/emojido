/*
 * Emojidō is a web application for creating and sharing emoji-made texts
 * Copyright (C) 2016 Juan Carlos Mejías Rodríguez
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package utils;

import controllers.WebJarAssets;
import org.webjars.WebJarAssetLocator;
import play.Configuration;
import play.Environment;
import play.Logger;
import play.api.Play;

import javax.imageio.ImageIO;
import javax.inject.Inject;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.regex.Pattern;

public class SpriteGenerator {

    // See https://dev.twitter.com/cards/types/summary-large-image
    // See https://developers.facebook.com/docs/sharing/best-practices#images
    static final int minWidth = 280;
    static final int minHeight = 200;

    static final int emojiWidth = 64;
    static final int emojiHeight = 64;
    static final int marginHorizontal = 5;
    static final int marginVertical = 5;

    @Inject
    public Environment environment;

    @Inject
    Configuration configuration;

    @Inject
    WebJarAssets webJarAssets;

    public InputStream generate (List<List<String>> emojiTextContent) {
        String webJarFilterExpr = configuration.getString(webJarAssets.WebjarFilterExprProp(), webJarAssets.WebjarFilterExprDefault());
        WebJarAssetLocator webJarAssetLocator = new WebJarAssetLocator(
                WebJarAssetLocator.getFullPathIndex(
                        Pattern.compile(webJarFilterExpr), environment.classLoader()));

        String version = webJarAssetLocator.getWebJars().get("emojione");

        BufferedImage spriteBufferedImage = new BufferedImage(calculateSpriteWidth(emojiTextContent), calculateSpriteHeight(emojiTextContent), BufferedImage.TYPE_INT_ARGB);
        Graphics graphics = spriteBufferedImage.getGraphics();

        final int[] lastX = {0};
        final int[] lastY = {0};
        emojiTextContent.forEach(line -> {
            line.forEach(shortname -> {
                String emojiImagePath = "";

                if (!shortname.equals("")) {
                    emojiImagePath = webJarAssetLocator.getFullPath("emojione", version + "/assets/png/" + ShortnameToFilename.getAsPNG(shortname));
                } else {
                    emojiImagePath = "public/images/blank-emoji.png";
                }

                BufferedImage emojiBufferedImage = null;
                try {
                    emojiBufferedImage = ImageIO.read(Play.current().resourceAsStream(emojiImagePath).get());
                } catch (IOException e) {
                    Logger.error("Can not read InputStream for " + emojiImagePath, e);
                }
                graphics.drawImage(emojiBufferedImage, lastX[0] + marginHorizontal, lastY[0] + marginVertical, null);
                lastX[0] += emojiWidth + (marginHorizontal * 2);
            });
            lastX[0] = 0;
            lastY[0] += emojiHeight + (marginVertical * 2);
        });

        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();

        try {
            ImageIO.write(spriteBufferedImage,"png", byteArrayOutputStream);
        } catch (IOException e) {
            Logger.error("Can not write BufferedImage to ByteArrayOutputStream", e);
        }
        graphics.dispose();

        return new ByteArrayInputStream(byteArrayOutputStream.toByteArray());
    }

    public int calculateSpriteWidth (List<List<String>> emojiTextContent) {
        int columns = emojiTextContent.stream().mapToInt(List::size).max().getAsInt();
        int width = columns * (emojiWidth + marginHorizontal * 2);
        return width >= minWidth ? width : minWidth;
    }

    public int calculateSpriteHeight (List<List<String>> emojiTextContent) {
        int rows = emojiTextContent.size();
        int height = rows * (emojiHeight + marginVertical * 2);
        return height >= minHeight ? height : minHeight;
    }

}
