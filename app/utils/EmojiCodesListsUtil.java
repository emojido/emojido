/*
 * Emojidō is a web application for creating and sharing emoji-made texts
 * Copyright (C) 2016 Juan Carlos Mejías Rodríguez
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package utils;

import org.apache.commons.lang3.StringUtils;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public class EmojiCodesListsUtil {
    public static List<List<String>> stringToMatrix (String string) {
        return Arrays
                .stream(string.split(System.lineSeparator()))
                .map(line -> stringToList(line)
                )
                .collect(Collectors.toList());
    }

    public static String matrixToString (List<List<String>> matrix) {
        return matrix
                .stream()
                .map(line -> listToString(line)
                )
                .collect(Collectors.joining(System.lineSeparator()));
    }

    public static List<String> stringToList (String string) {
        String[] array = StringUtils.substringsBetween(string, ":", ":");
        if (array != null) return Arrays.asList(array);
        else return Collections.EMPTY_LIST;
    }

    public static String listToString (List<String> list) {
        return list
                .stream()
                .collect(Collectors.joining("::", ":", ":"));
    }

}
