/*
 * Emojidō is a web application for creating and sharing emoji-made texts
 * Copyright (C) 2016 Juan Carlos Mejías Rodríguez
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package services;

import models.EmojiText;
import org.bson.Document;
import org.bson.types.ObjectId;
import uk.co.panaxiom.playjongo.PlayJongo;

import javax.inject.Inject;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.CompletionStage;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

import static java.util.concurrent.CompletableFuture.supplyAsync;

public class EmojiTextServiceImp implements EmojiTextService {
    @Inject
    public PlayJongo jongo;

    @Override
    public CompletionStage<Stream<EmojiText>> list(int pageSize, int pageNumber) {
        return supplyAsync(() -> {
            Iterable<EmojiText> iterable = jongo.getCollection("emojiText").find()
                    .sort("{creationDate: -1}")
                    .limit(pageSize)
                    .skip(pageNumber * pageSize)
                    .as(EmojiText.class);
            return StreamSupport.stream(iterable.spliterator(), true);
        });
    }

    @Override
    public CompletionStage<Stream<EmojiText>> listByCodes(List<String> codes, int pageSize, int pageNumber) {
        return supplyAsync(() -> {
            List<Document> conditions = codes.stream()
                    .map(code -> new Document("content", new Document("$elemMatch", new Document("$elemMatch", new Document("$eq", code)))))
                    .collect(Collectors.toList());
            Iterable<EmojiText> iterable = jongo.getCollection("emojiText")
                    .find("{$and: #}", conditions)
                    .sort("{creationDate: -1}")
                    .limit(pageSize)
                    .skip(pageNumber * pageSize)
                    .as(EmojiText.class);
            return StreamSupport.stream(iterable.spliterator(), true);
        });
    }

    @Override
    public CompletionStage<EmojiText> create(EmojiText emojiText) {
        return supplyAsync(() -> {
            jongo.getCollection("emojiText").save(emojiText);
            return emojiText;
        });
    }

    @Override
    public CompletionStage<Optional<EmojiText>> get(String id) {
        return supplyAsync(() -> Optional.ofNullable(jongo.getCollection("emojiText")
                .findOne(new ObjectId(id))
                .as(EmojiText.class)));
    }
}
