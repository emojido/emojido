/*
 * Emojidō is a web application for creating and sharing emoji-made texts
 * Copyright (C) 2016 Juan Carlos Mejías Rodríguez
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package handlers;

import resources.EmojiTextResource;
import models.EmojiText;
import services.EmojiTextService;

import javax.inject.Inject;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.CompletionStage;
import java.util.stream.Stream;

public class EmojiTextResourceHandler {
    private final EmojiTextService service;

    @Inject
    public EmojiTextResourceHandler(EmojiTextService service) {
        this.service = service;
    }

    public CompletionStage<Stream<EmojiTextResource>> list(int pageSize, int pageNumber) {
        return service
                .list(pageSize, pageNumber)
                .thenApplyAsync(emojiTextStream -> emojiTextStream.map(EmojiTextResource::new));
    }

    public CompletionStage<Stream<EmojiTextResource>> listByCodes(List<String> codes, int pageSize, int pageNumber) {
        return service
                .listByCodes(codes, pageSize, pageNumber)
                .thenApplyAsync(emojiTextStream -> emojiTextStream.map(EmojiTextResource::new));
    }

    public CompletionStage<EmojiTextResource> create(EmojiTextResource emojiTextResource) {
        EmojiText emojiText = new EmojiText();
        emojiText.title = emojiTextResource.getTitle();
        emojiText.content = emojiTextResource.getContent();

        return service
                .create(emojiText)
                .thenApplyAsync(EmojiTextResource::new);
    }

    public CompletionStage<Optional<EmojiTextResource>> get(String id) {
        return service
                .get(id)
                .thenApplyAsync(optionalEmojiText -> optionalEmojiText.map(EmojiTextResource::new));
    }
}
