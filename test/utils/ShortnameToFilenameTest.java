/*
 * Emojidō is a web application for creating and sharing emoji-made texts
 * Copyright (C) 2016 Juan Carlos Mejías Rodríguez
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package utils;

import org.junit.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.junit.Assert.assertThat;

public class ShortnameToFilenameTest {

    @Test
    public void get() throws Exception {
        String filename = ShortnameToFilename.get("100");

        assertThat("no filename", filename, notNullValue());
        assertThat("no filename", filename.isEmpty(), is(false));
        assertThat("wrong filename", filename, is("1f4af"));
    }

    @Test
    public void getAsPNG() throws Exception {
        String filename = ShortnameToFilename.getAsPNG("100");

        assertThat("no filename", filename, notNullValue());
        assertThat("no filename", filename.isEmpty(), is(false));
        assertThat("wrong filename", filename, is("1f4af.png"));
    }

    @Test
    public void getAsSVG() throws Exception {
        String filename = ShortnameToFilename.getAsSVG("100");

        assertThat("no filename", filename, notNullValue());
        assertThat("no filename", filename.isEmpty(), is(false));
        assertThat("wrong filename", filename, is("1f4af.svg"));
    }

}
