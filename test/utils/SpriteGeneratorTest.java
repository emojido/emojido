/*
 * Emojidō is a web application for creating and sharing emoji-made texts
 * Copyright (C) 2016 Juan Carlos Mejías Rodríguez
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package utils;

import models.EmojiText;
import org.junit.Test;
import play.Application;
import play.inject.guice.GuiceApplicationBuilder;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.junit.Assert.assertThat;
import static play.test.Helpers.running;

public class SpriteGeneratorTest {

    @Test
    public void generateSmall() {
        Application application = new GuiceApplicationBuilder().build();
        running (application, () -> {
            SpriteGenerator spriteGenerator = application.injector().instanceOf(SpriteGenerator.class);

            EmojiText emojiText = new EmojiText();
            emojiText.content = Arrays.asList(
                    Arrays.asList("book", "book"),
                    Arrays.asList("notebook", "notebook")
            );

            InputStream inputStream = spriteGenerator.generate(emojiText.content);

            assertThat("null stream", inputStream, notNullValue());

            try {
                BufferedImage im = ImageIO.read(inputStream);
                assertThat("image with wrong width", im.getWidth(), is(280));
                assertThat("image with wrong height", im.getHeight(), is(200));
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
    }

    @Test
    public void generateLarge() {
        Application application = new GuiceApplicationBuilder().build();
        running (application, () -> {
            SpriteGenerator spriteGenerator = application.injector().instanceOf(SpriteGenerator.class);

            EmojiText emojiText = new EmojiText();
            emojiText.content = Arrays.asList(
                    Arrays.asList("book", "book", "book", "book"),
                    Arrays.asList("notebook", "notebook", "notebook", "notebook"),
                    Arrays.asList("apple", "apple", "apple", "apple"),
                    Arrays.asList("peach", "peach", "peach", "peach")
            );

            InputStream inputStream = spriteGenerator.generate(emojiText.content);

            assertThat("null stream", inputStream, notNullValue());

            try {
                BufferedImage im = ImageIO.read(inputStream);
                assertThat("image with wrong width", im.getWidth(), is(296));
                assertThat("image with wrong height", im.getHeight(), is(296));
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
    }

    @Test
    public void calculateSpriteWidth() {
        Application application = new GuiceApplicationBuilder().build();
        running (application, () -> {
            SpriteGenerator spriteGenerator = application.injector().instanceOf(SpriteGenerator.class);

            EmojiText emojiText1 = new EmojiText();
            emojiText1.content = Arrays.asList(
                    Arrays.asList("book")
            );

            EmojiText emojiText2 = new EmojiText();
            emojiText2.content = Arrays.asList(
                    Arrays.asList("book", "book", "book", "book")
            );

            assertThat("wrong width", spriteGenerator.calculateSpriteWidth(emojiText1.content), is(280));
            assertThat("wrong width", spriteGenerator.calculateSpriteWidth(emojiText2.content), is(296));
        });
    }

    @Test
    public void calculateSpriteHeight() {
        Application application = new GuiceApplicationBuilder().build();
        running (application, () -> {
            SpriteGenerator spriteGenerator = application.injector().instanceOf(SpriteGenerator.class);

            EmojiText emojiText1 = new EmojiText();
            emojiText1.content = Arrays.asList(
                    Arrays.asList("book")
            );

            EmojiText emojiText2 = new EmojiText();
            emojiText2.content = Arrays.asList(
                    Arrays.asList("book"),
                    Arrays.asList("book"),
                    Arrays.asList("book"),
                    Arrays.asList("book")
            );

            assertThat("wrong width", spriteGenerator.calculateSpriteHeight(emojiText1.content), is(200));
            assertThat("wrong width", spriteGenerator.calculateSpriteHeight(emojiText2.content), is(296));
        });
    }

}
