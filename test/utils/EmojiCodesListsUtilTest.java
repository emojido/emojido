/*
 * Emojidō is a web application for creating and sharing emoji-made texts
 * Copyright (C) 2016 Juan Carlos Mejías Rodríguez
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package utils;

import org.junit.Test;

import java.util.Arrays;
import java.util.List;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

public class EmojiCodesListsUtilTest {

    @Test
    public void stringToMatrix() {
        String text = ":book: :100: :1234: "+System.lineSeparator()+":apple: :peach: "+System.lineSeparator()+":flag_cu:";
        List<List<String>> matrix = EmojiCodesListsUtil.stringToMatrix(text);

        assertThat("wrong parsed matrix values", matrix, is(Arrays.asList(Arrays.asList("book", "100", "1234"), Arrays.asList("apple", "peach"), Arrays.asList("flag_cu"))));
    }

    @Test
    public void matrixToString() throws Exception {
        List<List<String>> matrix = Arrays.asList(Arrays.asList("book", "100", "1234"), Arrays.asList("apple", "peach"), Arrays.asList("flag_cu"));
        String text = EmojiCodesListsUtil.matrixToString(matrix);

        assertThat("wrong parsed text values", text, is(":book::100::1234:"+System.lineSeparator()+":apple::peach:"+System.lineSeparator()+":flag_cu:"));
    }

    @Test
    public void stringToList() throws Exception {
        String text = "some random text :book: :100: someother text:1234: and some final text";
        List<String> list = EmojiCodesListsUtil.stringToList(text);

        assertThat("wrong parsed list values", list, is(Arrays.asList("book", "100", "1234")));
    }

    @Test
    public void listToString() throws Exception {
        List<String> list = Arrays.asList("book", "100", "1234");

        String text = EmojiCodesListsUtil.listToString(list);

        assertThat("wrong parsed list values", text, is(":book::100::1234:"));
    }

}
