/*
 * Emojidō is a web application for creating and sharing emoji-made texts
 * Copyright (C) 2016 Juan Carlos Mejías Rodríguez
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package services;

import models.EmojiText;
import org.jongo.MongoCollection;
import org.junit.Before;
import org.junit.Test;
import play.Application;
import play.inject.guice.GuiceApplicationBuilder;
import uk.co.panaxiom.playjongo.PlayJongo;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.stream.Collectors;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.fail;
import static play.test.Helpers.running;

public class EmojiTextServiceImpTest {

    @Before
    public void cleanDB () {
        Application application = new GuiceApplicationBuilder().build();
        running (application, () -> {
            PlayJongo playJongo = application.injector().instanceOf(PlayJongo.class);
            playJongo.getDatabase().dropDatabase();
        });
    }

    @Test
    public void list() throws Exception {
        Application application = new GuiceApplicationBuilder().build();
        running (application, () -> {
            PlayJongo playJongo = application.injector().instanceOf(PlayJongo.class);
            EmojiTextService emojiTextService = application.injector().instanceOf(EmojiTextService.class);

            try {
                emojiTextService.create(new EmojiText()).toCompletableFuture().get(5, TimeUnit.SECONDS);
                emojiTextService.create(new EmojiText()).toCompletableFuture().get(5, TimeUnit.SECONDS);
                emojiTextService.create(new EmojiText()).toCompletableFuture().get(5, TimeUnit.SECONDS);
                emojiTextService.create(new EmojiText()).toCompletableFuture().get(5, TimeUnit.SECONDS);
                emojiTextService.create(new EmojiText()).toCompletableFuture().get(5, TimeUnit.SECONDS);

                assertThat("found wrong emoji texts count", emojiTextService.list(3, 0).toCompletableFuture().get(5, TimeUnit.SECONDS).count(), is(3L));
                assertThat("found wrong emoji texts count", emojiTextService.list(3, 1).toCompletableFuture().get(5, TimeUnit.SECONDS).count(), is(2L));
                assertThat("found wrong emoji texts count", emojiTextService.list(3, 2).toCompletableFuture().get(5, TimeUnit.SECONDS).count(), is(0L));
            } catch (InterruptedException | ExecutionException | TimeoutException e) {
                fail();
            }
        });
    }

    @Test
    public void listByCodes() throws Exception {
        Application application = new GuiceApplicationBuilder().build();
        running (application, () -> {
            PlayJongo playJongo = application.injector().instanceOf(PlayJongo.class);
            EmojiTextService emojiTextService = application.injector().instanceOf(EmojiTextService.class);

            EmojiText emojiText1 = new EmojiText();
            emojiText1.content = Arrays.asList(
                    Arrays.asList("1", "2"),
                    Arrays.asList("3", "4"),
                    Arrays.asList("grinning", "grin")
            );

            EmojiText emojiText2 = new EmojiText();
            emojiText2.content = Arrays.asList(
                    Arrays.asList("3", "4"),
                    Arrays.asList("5", "6"),
                    Arrays.asList("grinning", "grin")
            );

            EmojiText emojiText3 = new EmojiText();
            emojiText3.content = Arrays.asList(
                    Arrays.asList("5", "6"),
                    Arrays.asList("7", "8"),
                    Arrays.asList("grinning", "grin")
            );

            try {
                emojiTextService.create(emojiText1).toCompletableFuture().get(5, TimeUnit.SECONDS);
                emojiTextService.create(emojiText2).toCompletableFuture().get(5, TimeUnit.SECONDS);
                emojiTextService.create(emojiText3).toCompletableFuture().get(5, TimeUnit.SECONDS);

                List<EmojiText> onlyOne = emojiTextService.listByCodes(Arrays.asList("1", "2"), 10, 0).toCompletableFuture().get(5, TimeUnit.SECONDS).collect(Collectors.toList());
                List<EmojiText> oneAndTwo = emojiTextService.listByCodes(Arrays.asList("3", "4"), 10, 0).toCompletableFuture().get(5, TimeUnit.SECONDS).collect(Collectors.toList());
                List<EmojiText> twoAndThree = emojiTextService.listByCodes(Arrays.asList("5", "6"), 10, 0).toCompletableFuture().get(5, TimeUnit.SECONDS).collect(Collectors.toList());
                List<EmojiText> onlyThree = emojiTextService.listByCodes(Arrays.asList("7", "8"), 10, 0).toCompletableFuture().get(5, TimeUnit.SECONDS).collect(Collectors.toList());
                List<EmojiText> allWithGrinning = emojiTextService.listByCodes(Arrays.asList("grinning", "grin"), 10, 0).toCompletableFuture().get(5, TimeUnit.SECONDS).collect(Collectors.toList());

                assertThat("found wrong emoji texts count", onlyOne.size(), is(1));
                assertThat("found wrong emoji texts count", oneAndTwo.size(), is(2));
                assertThat("found wrong emoji texts count", twoAndThree.size(), is(2));
                assertThat("found wrong emoji texts count", onlyThree.size(), is(1));
                assertThat("found wrong emoji texts count", allWithGrinning.size(), is(3));
            } catch (InterruptedException | ExecutionException | TimeoutException e) {
                fail();
            }
        });
    }

    @Test
    public void create() {
        Application application = new GuiceApplicationBuilder().build();
        running (application, () -> {
            PlayJongo playJongo = application.injector().instanceOf(PlayJongo.class);
            EmojiTextService emojiTextService = application.injector().instanceOf(EmojiTextService.class);
            MongoCollection mongoCollection = playJongo.getCollection("emojiText");

            EmojiText emojiText = new EmojiText();

            try {
                emojiTextService.create(emojiText).toCompletableFuture().get(5, TimeUnit.SECONDS);

                assertThat("emoji text not saved", mongoCollection.count(), is(1L));
            } catch (InterruptedException | TimeoutException | ExecutionException e) {
                fail();
            }
        });
    }

    @Test
    public void get() throws Exception {
        Application application = new GuiceApplicationBuilder().build();
        running (application, () -> {
            PlayJongo playJongo = application.injector().instanceOf(PlayJongo.class);
            EmojiTextService emojiTextService = application.injector().instanceOf(EmojiTextService.class);

            EmojiText emojiText = new EmojiText();

            try {
                emojiTextService.create(emojiText).toCompletableFuture().get(5, TimeUnit.SECONDS);

                Optional<EmojiText> retrievedEmojiText = emojiTextService.get(emojiText.id.toString()).toCompletableFuture().get(5, TimeUnit.SECONDS);
                Optional<EmojiText> inexistentEmojiText = emojiTextService.get("000000000000000000000000").toCompletableFuture().get(5, TimeUnit.SECONDS);

                assertThat("emoji text not found", retrievedEmojiText.isPresent(), is(true));
                assertThat("found wrong emoji text", retrievedEmojiText.get().id, equalTo(emojiText.id));
                assertThat("inexistent emoji text found", inexistentEmojiText.isPresent(), is(false));
            } catch (InterruptedException | TimeoutException | ExecutionException e) {
                fail();
            }
        });
    }

}
