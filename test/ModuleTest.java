/*
 * Emojidō is a web application for creating and sharing emoji-made texts
 * Copyright (C) 2016 Juan Carlos Mejías Rodríguez
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import org.junit.Test;
import play.Application;
import play.inject.guice.GuiceApplicationBuilder;
import services.EmojiTextService;
import services.EmojiTextServiceImp;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.junit.Assert.assertThat;
import static play.test.Helpers.running;

public class ModuleTest {

    @Test
    public void configure() {
        Application application = new GuiceApplicationBuilder().build();

        running (application, () -> {
            EmojiTextService emojiTextService = application.injector().instanceOf(EmojiTextService.class);

            assertThat("EmojiTextService has not been mapped", emojiTextService, notNullValue());
            assertThat("EmojiTextService has been mapped to a wrong type", emojiTextService instanceof EmojiTextServiceImp, is(true));
        });
    }

}
