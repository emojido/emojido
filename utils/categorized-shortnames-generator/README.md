# Emojione utility for Java

This utility creates a JSON file with shortcodes grouped by category, based on `emoji.json` file provided with Emojione.

### How to use

Copy the `emoji.json` file from the desired version of [Emojione](https://github.com/Ranks/emojione) next to `generate.js` file. Then run the utility:

```
npm install
node generate.js
```

It will generate the new file at `../../public/data/categorized.json`.
