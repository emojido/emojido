var util = require("util"),
    fs   = require("fs"),
    _    = require("underscore");

var emojis = _(require("./emoji.json")).map(function(data, shortname) {
    return {
        "shortname": shortname,
        "category": data.category,
        "emoji_order": parseInt(data.emoji_order)
    }
});

var categories = [
    {
        "name": "people",
        "label": "Smileys & People",
        "icon": "yum"
    },
    {
        "name": "nature",
        "label": "Animals & Nature",
        "icon": "hamster"
    },
    {
        "name": "food",
        "label": "Food & Drink",
        "icon": "pizza"
    },
    {
        "name": "activity",
        "label": "Activity",
        "icon": "basketball"
    },
    {
        "name": "travel",
        "label": "Travel & Places",
        "icon": "rocket"
    },
    {
        "name": "objects",
        "label": "Objects",
        "icon": "bulb"
    },
    {
        "name": "symbols",
        "label": "Symbols",
        "icon": "heartpulse"
    },
    {
        "name": "flags",
        "label": "Flags",
        "icon": "flag_gb"
    }
];

var categorized = [];
_(categories).each(function(category) {
    categorized.push ({
        "name": category.name,
        "label": category.label,
        "icon": category.icon,
        "shortnames": _.chain(emojis).where({"category": category.name}).sortBy('emoji_order').pluck('shortname').value()
    });
});

var output_path = "../../public/data/categorized.json";
fs.writeFileSync(output_path, JSON.stringify(categorized));
