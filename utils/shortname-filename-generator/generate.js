var util = require("util"),
    fs   = require("fs"),
    _    = require("underscore");


// Load emojis
var emojis = require("./emoji_strategy.json");

// Generate Java mapping
var mapping = _(emojis).map(function(data, shortname) {
    return '_shortnameToFilename.put("' + shortname + '", "' + data.unicode + '");';
}).join("\n        ");

// Generate Java class from template
var input  = fs.readFileSync("./ShortnameToFilename.java");
var output = _(input.toString()).template()({ mapping: mapping });

// Write Java class to file
var output_path = "../../app/utils/ShortnameToFilename.java";
fs.writeFileSync(output_path, output);

console.log("Generated " + output_path);