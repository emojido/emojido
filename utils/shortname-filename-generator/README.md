# Emojione utility for Java

### How to use

This utility creates a Java class which can convert [Emojione](https://github.com/Ranks/emojione) shortnames to their correspondent filenames (PNG and SVG):

```
ShortnameToFilename.get()
ShortnameToFilename.getAsPNG()
ShortnameToFilename.getAsSVG()
```

It uses `ShortnameToFilename.java` file as a template.

### How to re-generate mapping

Copy the `emoji_strategy.json` file from the desired version of [Emojione](https://github.com/Ranks/emojione) next to `generate.js` file. Then run the utility:

```
npm install
node generate.js
```

### Previous work

This utility is based on a similar one provided by [Emojione](https://github.com/Ranks/emojione) project for Android.