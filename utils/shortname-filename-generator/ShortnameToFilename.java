/*
 * Emojidō is a web application for creating and sharing emoji-made texts
 * Copyright (C) 2016 Juan Carlos Mejías Rodríguez
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package utils;

import java.util.HashMap;

public abstract class ShortnameToFilename
{
    private static final HashMap<String, String> _shortnameToFilename = new HashMap<String, String>();

    public static String get(String shortname)
    {
        return _shortnameToFilename.get(shortname);
    }

    public static String getAsPNG(String shortname)
    {
        return _shortnameToFilename.get(shortname).concat(".png");
    }

    public static String getAsSVG(String shortname)
    {
        return _shortnameToFilename.get(shortname).concat(".svg");
    }

    static {
        <%= mapping %>
    }

}
